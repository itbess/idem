# Import Python libs
import subprocess
import os

CODE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


def test_sls_shell():
    runpy = os.path.join(CODE_DIR, "run.py")
    tree = os.path.join(CODE_DIR, "tests", "sls")
    cmd = f"python3 {runpy} --sls-sources file://{tree} --sls simple"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
