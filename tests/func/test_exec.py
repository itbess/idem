# Import Python libs
import subprocess
import os

CODE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


def test_exec():
    runpy = os.path.join(CODE_DIR, "run.py")
    cmd = f"python3 {runpy} --exec test.ping"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
